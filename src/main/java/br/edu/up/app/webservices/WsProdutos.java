package br.edu.up.app.webservices;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import br.edu.up.app.dominio.ProdutoVendas;

@Component
public class WsProdutos {
	
	private final RestTemplate rest = new RestTemplate();
	private final String srvProdutos = "http://localhost:8081/produtos";

	public ProdutoVendas buscarProdutoPorId(Long idProduto) {
		Map<String, Long> params = new HashMap<>();
		params.put("id", idProduto);
		return rest.getForObject(srvProdutos + "/{id}?projection=produtoVendas", ProdutoVendas.class, params);
	}
}
