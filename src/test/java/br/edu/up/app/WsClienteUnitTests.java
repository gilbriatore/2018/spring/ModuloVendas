package br.edu.up.app;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.mvc.TypeConstrainedMappingJackson2HttpMessageConverter;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import br.edu.up.app.dominio.Cliente;

@RunWith(SpringRunner.class)
@SpringBootTest
public class WsClienteUnitTests {

	private static String CLIENTES_ENDPOINT = "http://localhost:8084/pessoas?projection=cliente";

	@Autowired
    @Qualifier("halJacksonHttpMessageConverter")
	private TypeConstrainedMappingJackson2HttpMessageConverter converter;
	
	public RestTemplate getRestTemplate() {
		RestTemplate rest = new RestTemplate();
		List<HttpMessageConverter<?>> converters = rest.getMessageConverters();
		converters.add(0, converter);
		rest.setMessageConverters(converters);
		return rest;
	}
	
	@Test
	public void listarClientesTest() {
		
		RestTemplate rest = getRestTemplate();
		ParameterizedTypeReference<PagedResources<Cliente>> type = new ParameterizedTypeReference<PagedResources<Cliente>>() {};
		ResponseEntity<PagedResources<Cliente>> response = rest.exchange(CLIENTES_ENDPOINT, HttpMethod.GET, null, type);
		PagedResources<Cliente> pResource = response.getBody();
		List<Cliente> lista = new ArrayList<>(pResource.getContent());
		
		for (Cliente cliente : lista) {
			System.out.println(cliente); 
		}
	}
	
	@Test
	public void buscarClientePorIDPOJOTest() {
		
		//Retorno como objeto
		RestTemplate rest = new RestTemplate();
		Cliente cliente = rest.getForObject(CLIENTES_ENDPOINT + "/1", Cliente.class);
		assertThat(cliente.getNome(), notNullValue());
		assertThat(cliente.getIdPessoa(), is(1L));

	}
}
